# Accounts API
## Table of Contents
- [Full Setup](#full-setup)
    - [Installation](#installation)
    - [Run](#run)
- [Usage](#usage)
    - [HTTP paths](#http-paths)
    - [Create new account](#create-new-account)
    - [Get accounts by id](#get-account-by-id)
    - [Get accounts by filter](#get-account-by-fiilter)
    - [Update account by id](#update-account-by-id)
    - [Delete account by id](#delete-account-by-id)

## Full Setup
### Installation

```bash
git clone https://gitlab.com/liadperetz/accounts-api.git

cd accounts-api

npm install
```

### Run 

```bash
npm start
```

## Usage
#### HTTP paths 

| METHOD | ENDPOINT                                                             | DESCRIPTION                                       |
| ------ | :--------------------------------------------------------------------| :----------------------------------------------   |
| POST   |  /api/accounts                                                          | Create new account                                    |
| GET    |  /api/accounts/:id                                                       | Get account by id                                     |
| GET    |  /api/accounts                                                     | Get accounts by query                                  |
| PUT    |  /api/accounts                                                           | Update account                                        |
| Delete    |  /api/accounts/:id                                                       | Delete account by id                                     |
**-------------------------------------------------------------------------------------------------------------------------------------**

### Create new account 
Create a new account

`POST /api/accounts`

#### Parameters
| Name     | Type   | Description                                                                                                        |
| :----    | :----- | :------------------------------------------------------------------------------------------------------------------|
| fullName | string | **Required**. The user's full name.                                                                                |
| nickname | string | **Required**. The user's nickname.                                                                                |
| age | number | **Required**. The user's age.                                                                                |


#### Example Input
```typescript
{
    "fullName": "Israel Israeli",
    "nickname": "GeverAl",
    "age": 30
}
```
 #### Response
 ```typescript
 "status": "200 OK"
{
    "fullName": "Israel Israeli",
    "nickname": "GeverAl",
    "age": 30,
    "createdAt": "2021-02-27T18:27:48.778Z",
    "updatedAt": "2021-02-27T18:27:48.778Z",
    "id": "603a8f2414d13a41c8a13ea7"
}
 ```
**-----------------------------------------------------------------------------------------------------------------------------------------**

### Get account by id
Get an account by his mongoose id.

`GET /api/accounts/:id`
#### Parameters
| Name   | Type   | Description                                                    |
| :----  | :----- | :------------------------------------------------------------  |
| id     | string | **Required**. The mongoose id of the account's document.       |


#### Request
`GET /api/accounts/603a8f2414d13a41c8a13ea7`
#### Response
```typescript
"status": "200 OK"
{
    "fullName": "Israel Israeli",
    "nickname": "GeverAl",
    "age": 30,
    "createdAt": "2021-02-27T18:27:48.778Z",
    "updatedAt": "2021-02-27T18:27:48.778Z",
    "id": "603a8f2414d13a41c8a13ea7"
}
```
**-----------------------------------------------------------------------------------------------------------------------------------------**
### Get accounts by filter
Get accounts by a filter.

`GET /api/accounts/`
#### Parameters
| Name   | Type   | Description                                                    |
| :----  | :----- | :------------------------------------------------------------  |
| fullName | string | The user's full name.                          |
| nickname | string | The user's nickname.        |
| age | number | The user's age.                   |


**At least 1 field**
`GET /api/accounts?age=30`

#### Response
```typescript
"status": "200 OK"
{[
    "fullName": "Israel Israeli",
    "nickname": "GeverAl",
    "age": 30,
    "createdAt": "2021-02-27T18:27:48.778Z",
    "updatedAt": "2021-02-27T18:27:48.778Z",
    "id": "603a8f2414d13a41c8a13ea7"
]}
```
**-----------------------------------------------------------------------------------------------------------------------------------------**
### Update account by id 
Update an account by id

`PUT /api/accounts/:id`

#### Parameters
| Name     | Type   | Description                  |
| :----    | :----- | :----------------------------|
| fullName | string | The user's full name.                          |
| nickname | string |The user's nickname.        |
| age | number | The user's age.                   |

**At least 1 field**

#### Example Input
` PUT /api/accounts/603a8f2414d13a41c8a13ea7`
```typescript
{
  "age": "50",
}
```
 #### Response
 ```typescript
 "status": "200 OK"
{
    "fullName": "Israel Israeli",
    "nickname": "GeverAl",
    "age": 50,
    "createdAt": "2021-02-27T18:27:48.778Z",
    "updatedAt": "2021-02-27T18:27:48.778Z",
    "id": "603a8f2414d13a41c8a13ea7"
}
 ```

**-----------------------------------------------------------------------------------------------------------------------------------------**

### Delete account by id
Get an account by his mongoose id.

`DELETE /api/accounts/:id`
#### Parameters
| Name   | Type   | Description                                                    |
| :----  | :----- | :------------------------------------------------------------  |
| id     | string | **Required**. The mongoose id of the account's document.       |


#### Request
`GET /api/accounts/603a8f2414d13a41c8a13ea7`
#### Response
```typescript
"status": "200 OK"
{
    "fullName": "Israel Israeli",
    "nickname": "GeverAl",
    "age": 30,
    "createdAt": "2021-02-27T18:27:48.778Z",
    "updatedAt": "2021-02-27T18:27:48.778Z",
    "id": "603a8f2414d13a41c8a13ea7"
}
```