import { Router, Request, Response } from 'express';
import accountsRouter from './accounts/accounts.router';

const router: Router = Router();

router.use('/api/accounts', accountsRouter);

router.use('/isalive', (_req: Request, res: Response) => {
  res.status(200).send('alive');
});

router.use('*', (_req: Request, res: Response) => {
  res.status(404).send('Error: Page doesn`t exist');
});

export default router;
