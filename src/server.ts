import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as helmet from 'helmet';
import * as http from 'http';
import * as morgan from 'morgan';
import { config } from './config';
import { logger } from './utils/logger/logger';
import { errorHandler } from './utils/errors/error-handler';
import router from './router';

export class Server {
  public app: express.Application;

  private server: http.Server;

  public static startServer(): Server {
    return new Server();
  }

  public closeServer(): void {
    this.server.close();
  }

  private constructor() {
    this.app = express();
    this.setMiddlewares();
    this.app.use(router);
    this.app.use(errorHandler);
    this.server = this.app.listen(config.express.port, () => {
      logger.log(`server has started on port: ${config.express.port}`);
    });
  }

  private setHeaders = (req: express.Request, res: express.Response,
    next: express.NextFunction): void => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Authorization, Origin, X-Requested-With, Content-Type');

    if (req.method === 'OPTIONS') {
      return res.status(200).end();
    }

    return next();
  };

  private setMiddlewares(): void {
    this.app.use(helmet());
    this.app.use(this.setHeaders);
    this.app.use(morgan('dev'));
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
  }
}
