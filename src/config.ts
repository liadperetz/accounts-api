export const config = {
  express: {
    port: process.env.APPLICATION_PORT || 5000,
  },
  mongo: {
    url: process.env.MONGO_CONNECTION_STRING || `mongodb://${process.env.MONGO_CONTAINER_NAME || 'localhost'}:${process.env.MONGO_PORT || 27017}/${process.env.MONGO_DB_NAME || 'accounts'}`,
  },
};
