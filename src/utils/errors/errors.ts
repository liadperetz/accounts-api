export class ServiceError extends Error {
  constructor(public statusCode: number = 500, message: string, public name: string) {
    super(message);
  }
}

export class AccountNotFoundError extends ServiceError {
  constructor() {
    super(404, 'Account Wasn\'t found', 'AccountNotFoundError');
  }
}

export class ValidationError extends ServiceError {
  constructor() {
    super(400, 'Validation Error', 'ValidationError');
  }
}
