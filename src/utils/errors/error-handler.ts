import { Request, Response, NextFunction } from 'express';
import { ValidationError } from 'joi';
import { ServiceError } from './errors';

export const errorHandler = (error: Error, _req: Request, res: Response, next: NextFunction): void => {
  if (error instanceof ValidationError) {
    res.status(400).send({
      type: error.name,
      message: error.message,
    });
  } else if (error instanceof ServiceError) {
    res.status(error.statusCode).send({
      type: error.name,
      message: error.message,
    });
  } else {
    res.status(500).send({
      type: error.name,
      message: error.message,
    });
  }
  next();
};
