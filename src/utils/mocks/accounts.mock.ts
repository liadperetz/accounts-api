import { IAccount } from '../../accounts/accounts.interface';

export const validAccount: IAccount = {
  fullName: 'Israel Israeli',
  nickname: 'israel',
  age: 40,
};

export const validFilter: Partial<IAccount> = {
  nickname: 'israel',
};

export const dataToUpdate: Partial<IAccount> = {
  age: 50,
};

export const doesntExistAccountFilter: Partial<IAccount> = {
  age: 4564654,
};
