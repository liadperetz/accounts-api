import * as mongoose from 'mongoose';
import { config } from './config';
import { Server } from './server';
import { logger } from './utils/logger/logger';

async function connectToDB(): Promise<void> {
  await mongoose.connect(config.mongo.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  mongoose.connection.on('connecting', () => {
    logger.log(`mongoDB on url ${config.mongo.url} is connecting...`);
  });

  mongoose.connection.on('connected', () => {
    logger.log(`mongoDB on url ${config.mongo.url} is connected`);
  });

  mongoose.connection.on('error', () => {
    logger.log(`mongoDB on url ${config.mongo.url} has an error`);
    process.exit(1);
  });

  mongoose.connection.on('disconnected', () => {
    logger.log(`mongoDB on url ${config.mongo.url} has been disconnected`);
    process.exit(1);
  });
}

(async (): Promise<void> => {
  await connectToDB();
  Server.startServer();
})();
