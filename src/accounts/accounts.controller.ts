import { Request, Response } from 'express';
import { AccountsManager } from './accounts.manager';

export class AccountsController {
  public static async getAccountById(req: Request, res: Response): Promise<void> {
    res.json(await AccountsManager.getAccountById(req.params.id));
  }

  public static async getAccountsByFilter(req: Request, res: Response): Promise<void> {
    res.json(await AccountsManager.getAccountsByFilter(req.query));
  }

  public static async updateAccount(req: Request, res: Response): Promise<void> {
    res.json(await AccountsManager.updateAccount(req.params.id, req.body));
  }

  public static async createAccount(req: Request, res: Response): Promise<void> {
    res.json(await AccountsManager.createAccount(req.body));
  }

  public static async deleteAccountById(req: Request, res: Response): Promise<void> {
    res.json(await AccountsManager.deleteAccountById(req.params.id));
  }
}
