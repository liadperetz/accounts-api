import { AccountNotFoundError } from '../utils/errors/errors';
import { IAccount } from './accounts.interface';
import { AccountsRepository } from './accounts.repository';

export class AccountsManager {
  public static async getAccountById(id: string): Promise<IAccount> {
    const account = await AccountsRepository.getAccountById(id);
    if (!account) throw new AccountNotFoundError();
    return account;
  }

  public static async getAccountsByFilter(filter: Partial<IAccount>): Promise<IAccount[]> {
    return AccountsRepository.getAccountsByFilter(filter);
  }

  public static async updateAccount(id: string, dataToUpdate: Partial<IAccount>): Promise<IAccount> {
    const accounts = await AccountsRepository.updateAccount(id, dataToUpdate);
    if (!accounts) throw new AccountNotFoundError();
    return accounts;
  }

  public static async createAccount(accountToCreate: IAccount): Promise<IAccount> {
    return AccountsRepository.createAccount(accountToCreate);
  }

  public static async deleteAccountById(id: string): Promise<IAccount> {
    const account = await AccountsRepository.deleteAccountById(id);
    if (!account) throw new AccountNotFoundError();
    return account;
  }
}
