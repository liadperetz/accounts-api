export interface IAccount {
  id?: string,
  fullName: string,
  nickname: string,
  age: number,
}
