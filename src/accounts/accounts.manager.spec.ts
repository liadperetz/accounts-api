import * as mongoose from 'mongoose';
import { config } from '../config';
import { AccountsManager } from './accounts.manager';
import { validAccount, validFilter, dataToUpdate } from '../utils/mocks/accounts.mock';
import { AccountNotFoundError } from '../utils/errors/errors';

describe('Accounts Manager module', () => {
  beforeAll(async () => {
    await mongoose.connect(config.mongo.url, {
      useFindAndModify: false,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
  });

  afterEach(async () => {
    await mongoose.connection.db.dropDatabase();
  });

  afterAll(async () => {
    await mongoose.connection.close();
  });

  describe('Find account by id', () => {
    test('Should return account when id valid and exist', async () => {
      const createdAccount = await AccountsManager.createAccount(validAccount);
      const { id } = createdAccount;

      expect(id).toBeDefined();
      const account = await AccountsManager.getAccountById(id as string);
      expect(account).toEqual(expect.objectContaining(validAccount));
    });

    test('Should throw error when account doesn\'t exist', async () => {
      const getAccountPromise = AccountsManager.getAccountById(mongoose.Types.ObjectId().toHexString());
      expect(getAccountPromise).rejects.toThrowError(AccountNotFoundError);
    });
  });

  describe('Find account by filter', () => {
    test('Should return an account by a query', async () => {
      await AccountsManager.createAccount(validAccount);
      const accounts = await AccountsManager.getAccountsByFilter(validFilter);
      expect(accounts).toHaveLength(1);
      expect(accounts[0]).toEqual(expect.objectContaining(validAccount));
    });

    test('Should return empty array when couln\'t find any matching accounts', async () => {
      const accounts = await AccountsManager.getAccountsByFilter(validFilter);
      expect(accounts).toHaveLength(0);
    });
  });

  describe('Create account', () => {
    test('Should create an account', async () => {
      const createdAccount = await AccountsManager.createAccount(validAccount);
      expect(createdAccount).toEqual(expect.objectContaining(validAccount));
    });
  });

  describe('Update account by id', () => {
    test('Should update the account', async () => {
      const createdAccount = await AccountsManager.createAccount(validAccount);
      const { id } = createdAccount;

      expect(id).toBeDefined();
      const updatedAccount = await AccountsManager.updateAccount(id as string, dataToUpdate);
      expect(updatedAccount).toEqual(expect.objectContaining({ ...validAccount, ...dataToUpdate }));
    });

    test('Should throw error if account wasn\'t found', async () => {
      const updateAccountPromise = AccountsManager.updateAccount(mongoose.Types.ObjectId().toHexString(), dataToUpdate);
      expect(updateAccountPromise).rejects.toThrowError(AccountNotFoundError);
    });
  });

  describe('Delete account by id', () => {
    test('Should return account when id is valid and exists', async () => {
      const createdAccount = await AccountsManager.createAccount(validAccount);
      const { id } = createdAccount;

      expect(id).toBeDefined();
      const account = await AccountsManager.deleteAccountById(id as string);
      expect(account).toEqual(expect.objectContaining(validAccount));
    });

    test('Should throw error when account doesn\'t exist', async () => {
      const getAccountPromise = AccountsManager.deleteAccountById(mongoose.Types.ObjectId().toHexString());
      expect(getAccountPromise).rejects.toThrowError(AccountNotFoundError);
    });
  });
});
