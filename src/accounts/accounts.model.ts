import * as mongoose from 'mongoose';
import { IAccount } from './accounts.interface';

const AccountsSchema: mongoose.Schema = new mongoose.Schema({
  fullName: {
    type: String,
    required: true,
  },
  nickname: {
    type: String,
    required: true,
  },
  age: {
    type: Number,
    required: true,
  },
}, {
  toJSON: {
    virtuals: true,
    transform(_doc, ret): void {
      // eslint-disable-next-line no-underscore-dangle
      delete ret._id;
    },
  },
  versionKey: false,
  id: true,
  timestamps: { updatedAt: true, createdAt: true },
});

export const AccountsModel = mongoose.model<IAccount & mongoose.Document>('accounts', AccountsSchema);
