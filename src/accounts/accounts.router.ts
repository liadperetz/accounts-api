import * as express from 'express';
import { AccountsController } from './accounts.controller';
import { wrapController } from '../utils/wrapper';
import { ValidateRequest } from '../utils/joi';
import {
  canGetById,
  canGetByFilter,
  canUpdateAccount,
  canCreateAccount,
} from './validator/accounts.schema.validator';

const accountsRouter: express.Router = express.Router();

accountsRouter.get('/:id', ValidateRequest(canGetById), wrapController(AccountsController.getAccountById));

accountsRouter.get('/', ValidateRequest(canGetByFilter), wrapController(AccountsController.getAccountsByFilter));

accountsRouter.put('/:id', ValidateRequest(canUpdateAccount), wrapController(AccountsController.updateAccount));

accountsRouter.post('/', ValidateRequest(canCreateAccount), wrapController(AccountsController.createAccount));

accountsRouter.delete('/', ValidateRequest(canGetById), wrapController(AccountsController.deleteAccountById));

export default accountsRouter;
