import { IAccount } from './accounts.interface';
import { AccountsModel } from './accounts.model';

export class AccountsRepository {
  public static async getAccountById(id: string): Promise<IAccount|null> {
    return AccountsModel.findById(id).exec();
  }

  public static async getAccountsByFilter(filter: Partial<IAccount>): Promise<IAccount[]> {
    return AccountsModel.find(filter).exec();
  }

  public static async updateAccount(id: string, dataToUpdate: Partial<IAccount>): Promise<IAccount | null> {
    return AccountsModel.findByIdAndUpdate(id, dataToUpdate, { new: true }).exec();
  }

  public static async createAccount(accountToCreate: IAccount): Promise<IAccount> {
    return AccountsModel.create(accountToCreate);
  }

  public static async deleteAccountById(id: string): Promise<IAccount|null> {
    return AccountsModel.findByIdAndRemove(id).exec();
  }
}
