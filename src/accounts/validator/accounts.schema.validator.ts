import * as Joi from 'joi';

export const canGetById = Joi.object({
  body: {},
  query: {},
  params: {
    id: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
  },
});

export const canGetByFilter = Joi.object({
  body: {},
  params: {},
  query: Joi.object({
    fullName: Joi.string().min(1),
    nickname: Joi.string().min(1),
    age: Joi.number().greater(0),
  }).required().min(1),
});

export const canCreateAccount = Joi.object({
  body: Joi.object({
    fullName: Joi.string().min(1).required(),
    nickname: Joi.string().min(1).required(),
    age: Joi.number().greater(0).required(),
  }).required(),
  query: {},
  params: {
  },
});

export const canUpdateAccount = Joi.object({
  body: Joi.object({
    fullName: Joi.string().min(1),
    nickname: Joi.string().min(1),
    age: Joi.number().greater(0),
  }).required().min(1),
  query: {},
  params: {
    id: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
  },
});
