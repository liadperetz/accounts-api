import * as mongoose from 'mongoose';
import * as request from 'supertest';
import { AccountsManager } from './accounts.manager';
import { config } from '../config';
import {
  validAccount,
  validFilter,
  dataToUpdate,
  doesntExistAccountFilter,
} from '../utils/mocks/accounts.mock';
import { Server } from '../server';
import { AccountNotFoundError, ValidationError } from '../utils/errors/errors';

let server: Server;
const path = '/api/accounts';
const fakeId = mongoose.Types.ObjectId().toHexString();
const invalidId = '1234';

describe('Accounts Router module', () => {
  beforeAll(async () => {
    await mongoose.connect(config.mongo.url, {
      useFindAndModify: false,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    server = Server.startServer();
  });

  afterEach(async () => {
    await mongoose.connection.db.dropDatabase();
  });

  afterAll(async () => {
    await mongoose.connection.db.dropDatabase();
    await mongoose.connection.close();
    server.closeServer();
  });

  describe('GET /api/accounts/:id', () => {
    describe('Valid input', () => {
      test('Should return an account when id is valid and exists', async (done) => {
        const createdAccount = await AccountsManager.createAccount(validAccount);
        request(server.app)
          .get(`${path}/${createdAccount?.id}`)
          .expect(200)
          .expect('content-type', /json/)
          .end((error: Error, res: request.Response) => {
            expect(error).toBeNull();
            expect(res).toBeDefined();
            expect(res.body).toEqual(expect.objectContaining(validAccount));
            done();
          });
      });
    });
    describe('Invalid input', () => {
      test('Should throw AccountNotFoundError if account with that id doesn\'t exist', async (done) => {
        request(server.app)
          .get(`${path}/${fakeId}`)
          .expect(404)
          .expect('content-type', /json/)
          .end((error: Error, res: request.Response) => {
            expect(error).toBeNull();
            expect(res).toBeDefined();
            expect(res.body).toHaveProperty('message', new AccountNotFoundError().message);
            done();
          });
      });
      test('Should throw ValidationError if the id is invalid', async (done) => {
        request(server.app)
          .get(`${path}/${invalidId}`)
          .expect(400)
          .expect('content-type', /json/)
          .end((error: Error, res: request.Response) => {
            expect(error).toBeNull();
            expect(res).toBeDefined();
            expect(res.body).toHaveProperty('type', new ValidationError().name);
            done();
          });
      });
    });
  });

  describe('GET /api/accounts/', () => {
    describe('Valid query', () => {
      test('Should return account in array when query is valid and match an existing account', async (done) => {
        await AccountsManager.createAccount(validAccount);
        request(server.app)
          .get(`${path}`).query(validFilter)
          .expect(200)
          .expect('content-type', /json/)
          .end((error: Error, res: request.Response) => {
            expect(error).toBeNull();
            expect(res).toBeDefined();
            expect(res.body).toHaveLength(1);
            expect(res.body[0]).toEqual(expect.objectContaining(validAccount));
            done();
          });
      });
      test('Should return empty array when there aren\'t any matching accounts', async (done) => {
        await AccountsManager.createAccount(validAccount);
        request(server.app)
          .get(`${path}`).query(doesntExistAccountFilter)
          .expect(200)
          .expect('content-type', /json/)
          .end((error: Error, res: request.Response) => {
            expect(error).toBeNull();
            expect(res).toBeDefined();
            expect(res.body).toHaveLength(0);
            done();
          });
      });
    });
    describe('Invalid filter', () => {
      test('Should throw ValidationError if query is invalid', async (done) => {
        request(server.app)
          .get(`${path}`).query({})
          .expect(400)
          .expect('content-type', /json/)
          .end((error: Error, res: request.Response) => {
            expect(error).toBeNull();
            expect(res).toBeDefined();
            expect(res.body).toHaveProperty('type', new ValidationError().name);
            done();
          });
      });
    });
  });

  describe('POST /api/accounts/', () => {
    describe('Valid account data', () => {
      test('Should create an account', async (done) => {
        request(server.app)
          .post(`${path}`).send(validAccount)
          .expect(200)
          .expect('content-type', /json/)
          .end((error: Error, res: request.Response) => {
            expect(error).toBeNull();
            expect(res).toBeDefined();
            expect(res.body).toEqual(expect.objectContaining(validAccount));
            done();
          });
      });
    });

    describe('Invalid account data', () => {
      test('Should throw error if account data is invalid', async (done) => {
        const { age, ...invalidAccount } = validAccount;
        request(server.app)
          .post(`${path}`).send(invalidAccount)
          .expect(400)
          .expect('content-type', /json/)
          .end((error: Error, res: request.Response) => {
            expect(error).toBeNull();
            expect(res).toBeDefined();
            expect(res).toHaveProperty('body');
            expect(res.body).toHaveProperty('type', ValidationError.name);
            expect(res.body).toHaveProperty('message');
            done();
          });
      });
    });
  });

  describe('PUT /api/accounts/', () => {
    describe('Valid account data to update and existing account id', () => {
      test('Should update the account', async (done) => {
        const createdAccount = await AccountsManager.createAccount(validAccount);
        request(server.app)
          .put(`${path}/${createdAccount.id}`).send(dataToUpdate)
          .expect(200)
          .expect('content-type', /json/)
          .end((error: Error, res: request.Response) => {
            expect(error).toBeNull();
            expect(res).toBeDefined();
            expect(res.body).toEqual(expect.objectContaining({ ...validAccount, ...dataToUpdate }));
            done();
          });
      });
    });

    describe('Invalid data', () => {
      test('Should throw ValidationError if there isn\'t any data to update (empty body)', async (done) => {
        const createdAccount = await AccountsManager.createAccount(validAccount);
        request(server.app)
          .put(`${path}/${createdAccount.id}`).send({})
          .expect(400)
          .expect('content-type', /json/)
          .end((error: Error, res: request.Response) => {
            expect(error).toBeNull();
            expect(res).toBeDefined();
            expect(res.body).toHaveProperty('type', ValidationError.name);
            expect(res.body).toHaveProperty('message');
            done();
          });
      });
      test('Should throw AccountNotFoundError if account with that id doesn\'t exist', async (done) => {
        request(server.app)
          .get(`${path}/${fakeId}`)
          .expect(404)
          .expect('content-type', /json/)
          .end((error: Error, res: request.Response) => {
            expect(error).toBeNull();
            expect(res).toBeDefined();
            expect(res.body).toHaveProperty('message', new AccountNotFoundError().message);
            done();
          });
      });
    });
  });

  describe('DELETE /api/accounts/:id', () => {
    describe('Valid input', () => {
      test('Should return an account when id is valid and exists', async (done) => {
        const createdAccount = await AccountsManager.createAccount(validAccount);
        request(server.app)
          .get(`${path}/${createdAccount?.id}`)
          .expect(200)
          .expect('content-type', /json/)
          .end((error: Error, res: request.Response) => {
            expect(error).toBeNull();
            expect(res).toBeDefined();
            expect(res.body).toEqual(expect.objectContaining(validAccount));
            done();
          });
      });
    });
    describe('Invalid input', () => {
      test('Should throw AccountNotFoundError if account with that id doesn\'t exist', async (done) => {
        request(server.app)
          .get(`${path}/${fakeId}`)
          .expect(404)
          .expect('content-type', /json/)
          .end((error: Error, res: request.Response) => {
            expect(error).toBeNull();
            expect(res).toBeDefined();
            expect(res.body).toHaveProperty('message', new AccountNotFoundError().message);
            done();
          });
      });
      test('Should throw ValidationError if the id is invalid', async (done) => {
        request(server.app)
          .get(`${path}/${invalidId}`)
          .expect(400)
          .expect('content-type', /json/)
          .end((error: Error, res: request.Response) => {
            expect(error).toBeNull();
            expect(res).toBeDefined();
            expect(res.body).toHaveProperty('type', new ValidationError().name);
            done();
          });
      });
    });
  });

  describe('/isalive', () => {
    test('get /isalive', async (done) => {
      request(server.app)
        .get('/isalive')
        .expect(200)
        .expect('content-type', /html/)
        .end((error: Error, res: request.Response) => {
          expect(error).toBeNull();
          expect(res).toBeDefined();
          expect(res.text).toEqual('alive');
          done();
        });
    });
  });

  describe('all other routes', () => {
    test('Should throw error if route wasn\'t found ', async (done) => {
      request(server.app)
        .get('/fakeroute')
        .expect(404)
        .expect('content-type', /html/)
        .end((error: Error, res: request.Response) => {
          expect(error).toBeNull();
          expect(res).toBeDefined();
          expect(res.text).toEqual('Error: Page doesn`t exist');
          done();
        });
    });
  });
});
